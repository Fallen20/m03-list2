package list1_parte2;

import java.text.Collator;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

public class Waypoint_Dades implements Comparable{
	

	private int id;                     
	private String nom;
	private int[] coordenades;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
	private static int idNext=0;//esto hace que se incremente cada vez que creas uno
	
	public Waypoint_Dades(String nom, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		this.id = 0;
		this.nom = nom;
		this.coordenades = new int[] {0,0,0};
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
		//idNext++;
	}
	
	public int getId() {return id;}
	public String getNom() {return nom;}
	public int[] getCoordenades() {return coordenades;}
	public boolean isActiu() {return actiu;}
	public LocalDateTime getDataCreacio() {return dataCreacio;}
	public LocalDateTime getDataAnulacio() {return dataAnulacio;}
	public LocalDateTime getDataModificacio() {return dataModificacio;}
	
	public void setId(int id) {this.id = id;}
	public void setNom(String nom) {this.nom = nom;}
	public void setCoordenades(int[] coordenades) {this.coordenades = coordenades;}
	public void setActiu(boolean actiu) {this.actiu = actiu;}
	public void setDataCreacio(LocalDateTime dataCreacio) {this.dataCreacio = dataCreacio;}
	public void setDataAnulacio(LocalDateTime dataAnulacio) {this.dataAnulacio = dataAnulacio;}
	public void setDataModificacio(LocalDateTime dataModificacio) {this.dataModificacio = dataModificacio;}
	
	
	@Override
	public String toString() {
		DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int coord1Obj=(int)Math.pow(this.coordenades[0], 2);
		int coord2Obj=(int)Math.pow(this.coordenades[1], 2);
		int coord3Obj=(int)Math.pow(this.coordenades[2], 2);
		
		int sumaObj=coord1Obj+coord2Obj+coord3Obj;
		
		String finale="WAYPOINT "+this.id+":\n"
					+"\tnom = "+this.nom+"\n"
					+"\tcoordenades (x,y,z) = ("+this.coordenades[0]+","+this.coordenades[1]+","+this.coordenades[2]+")"
					+" (distancia= "+sumaObj+")\n"
					+"\tactiu = "+this.actiu+"\n";
		if(!this.dataCreacio.equals(null)) {
			finale+="\tdataCreacio = "+this.dataCreacio.format(fecha)+"\n";
		}
		else {finale+="\tdataCreacio = NULL\n";}
		
		if(this.dataAnulacio!=null) {
			finale+="\tdataAnulacio = "+this.dataAnulacio.format(fecha)+"\n";
		}
		else {finale+="\tdataAnulacio = NULL\n";}
		
		if(this.dataModificacio!=null) {
			finale+="\tdataModificacio = "+this.dataModificacio.format(fecha)+"\n";
		}
		else {finale+="\tdataModificacio = NULL\n";}
					
		
		return finale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(coordenades);
		result = prime * result + Objects.hash(nom);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Waypoint_Dades other = (Waypoint_Dades) obj;
		return Arrays.equals(coordenades, other.coordenades) && Objects.equals(nom, other.nom);
	}
    
	

	@Override
	public int compareTo(Object obj) {
		///0 es igual
		Waypoint_Dades o=(Waypoint_Dades) obj;
		
				int result=0;
				if(Arrays.equals(this.coordenades, o.coordenades)==true) {
					
					
					//manera profe
					/*Collator instance = Collator.getInstance(new Locale("es"));
					instance.setStrength(Collator.TERTIARY);
					result = instance.compare(this.getNom(), o.getNom());*/
					
					//MANERA 2
					String n1= this.getNom().toUpperCase();
					n1 = Normalizer.normalize(n1, Normalizer.Form.NFD);

					String n2=o.getNom().toUpperCase();//todo a mayus para ignorar mayus y minus
					n2 = Normalizer.normalize(n2, Normalizer.Form.NFD);
					//esta manera hace lo mismo que lo del profe
					
					result = n1.compareTo(n2);//si son iguales solo mira si el nombre es igual*/
				}
				else {
					int coord1=(int)Math.pow(coordenades[0], 2);
					int coord2=(int)Math.pow(coordenades[1], 2);
					int coord3=(int)Math.pow(coordenades[2], 2);
					
					int coord1Obj=(int)Math.pow(o.coordenades[0], 2);
					int coord2Obj=(int)Math.pow(o.coordenades[1], 2);
					int coord3Obj=(int)Math.pow(o.coordenades[2], 2);
					
					int sumaObj=coord1Obj+coord2Obj+coord3Obj;
					int suma=coord1+coord2+coord3;

					if(sumaObj>suma) {result=-1;}
					else if(sumaObj<suma) {result=1;}
					else {result=0;}
					
				}
				
				return result;
	}


    
    
}
